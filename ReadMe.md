## Infant Skin Infections Advisor
A sample expert system to diagnose common skin infections in babies built using [expertise2go](http://www.expertise2go.com/).

### System Overview
This system should be able to assist you diagnose various skin infections in infnants i.e children between the ages of 0-4 years and give appropriate advice.
The following infections are covered:
```
i) Commom Cold sores
ii) Nappy Rush
iii) Measles
iv) Cheacken Pox
v) Baby Acne
vi) Milia
vii) RingWorms
vii) Jaundice
viii) Stye
ix) Insect Bites
x) Umbilical Hernia
xi) Oral Thrush

```

### How to run the program
##### Requirements
You'll need to have [Java](https://java.com/en/download/help/download_options.xml) installed in the machine

##### Steps
Clone this repo `git clone https://gitlab.com/MwendwaC/expertise2go-Infant-skinInfections-diagnosis.git` or
                `git clone git@gitlab.com:MwendwaC/expertise2go-Infant-skinInfections-diagnosis.git` if using ssh.

edit `expertise2go-Infant-skinInfections-diagnosis/e2g3g/infant_skinadvisor.sh` if on unix(OSX,Linux) and `expertise2go-Infant-skinInfections-diagnosis/e2g3g/infant_skinadvisor.bat` if on windows.  and add the full path to the file `expertise2go-Infant-skinInfections-diagnosis/e2g3g/infant_skin.kb` as -kb option(currently set as `file:///Users/Clement/Documents/expertise2go-Infant-skinInfections-diagnosis/e2g3g/infant_skin.kb`).

Then navigate to the repo `cd expertise2go-Infant-skinInfections-diagnosis/` and run `./e2g3g/infant_skinadvisor.sh` or .bat if on windows. This should open a window with the expert system.